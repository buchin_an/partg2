package com.company;

import com.company.model.*;
import com.company.model.information.OrganizationInfo;

import java.util.ArrayList;
import java.util.List;

public class FinanceSystem {

    private final List<Creditable> creditables;
    private final List<Exchangeble> exchangebles;
    private final List<Sender> senders;
    private final List<Depositable> depositables;

    private FinanceSystem() {
        creditables = new ArrayList<>();
        exchangebles = new ArrayList<>();
        senders = new ArrayList<>();
        depositables = new ArrayList<>();
    }


    public void convert(int amount, int choice, boolean isUA) {
        Exchangeble bestOrganization = null;
        double bestValue = 0;
        for (Exchangeble exchangeble : exchangebles) {
            double converted = exchangeble.convert(amount, choice, isUA);
            if (bestValue < converted) {
                bestValue = converted;
                bestOrganization = exchangeble;
            }
        }
        printOrganizationInfo(bestOrganization);
        System.out.println("Best amount: " + bestValue);
    }

    public void credit(int amount, int number) {
        Creditable bestOrganization = null;
        double bestValue = Double.MAX_VALUE;
        for (Creditable organization : creditables) {
            double payment = organization.credit(amount, number);
            if (bestValue > payment) {
                bestValue = payment;
                bestOrganization = organization;
            }
        }
        printOrganizationInfo(bestOrganization);
        System.out.println("best rate: " + bestValue);
    }


    public void deposite(int amount, int number) {
        Depositable bestOrganization = null;
        double bestValue = 0;
        for (Depositable organization : depositables) {
            double deposited = organization.deposite(amount, number);
            if (bestValue < deposited) {
                bestValue = deposited;
                bestOrganization = organization;
            }
        }
        printOrganizationInfo(bestOrganization);
        System.out.println("best month rate " + bestValue);
    }

    public void send(int amount) {
        Sender bestOrganization = null;
        double bestValue = 0;
        for (Sender organization : senders) {
            double sent = organization.send(amount);
            if (bestValue < sent) {
                bestValue = sent;
                bestOrganization = organization;
            }
        }
        printOrganizationInfo(bestOrganization);
        System.out.println("delivered " + bestValue);
    }

    private void printOrganizationInfo(Object bestOrganization) {
        if (bestOrganization == null) {
            System.out.println("There isn't appropriate organization");
            return;
        }
        OrganizationInfo organizationInfo = ((Organization) bestOrganization).getOrganizationInfo();
        System.out.println("Best Organization " + organizationInfo.formattedValue());
    }

    public List<Creditable> getCreditables() {
        return creditables;
    }

    public List<Exchangeble> getExchangebles() {
        return exchangebles;
    }

    public List<Sender> getSenders() {
        return senders;
    }

    public List<Depositable> getDepositables() {
        return depositables;
    }

}
