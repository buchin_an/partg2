package com.company;

import com.company.model.impl.*;
import com.company.model.information.BaseInfo;
import com.company.model.information.OpenEndFundInfo;

import java.util.Arrays;
import java.util.List;

public final class FinanceSystemGenerator {

    public FinanceSystem generate() {

        FinanceSystem financeSystem = new FinanceSystem();

        Bank privateBank = createBank("Private bank", "Somewhere 1", 25, 24, 0.4f);
        Bank publicBank = createBank("Private bank", "Somewhere 1", 25, 24, 0.4f);
        Bank protectedBank = createBank("Private bank", "Somewhere 1", 25, 24, 0.4f);
        List<Bank> banks = Arrays.asList(privateBank, publicBank, protectedBank);

        CurrencyExchanger alphaExchanger = createExchanger("alpha", "Somewhere 4", 20.5f, 28.6f, 0.7f);
        CurrencyExchanger betaExchanger = createExchanger("beta", "Somewhere 5", 21.1f, 28.3f, 0.7f);
        CurrencyExchanger gammaExchanger = createExchanger("gamma", "Somewhere 6", 27f, 28.1f, 0.7f);
        List<CurrencyExchanger> currencyExchangers = Arrays.asList(alphaExchanger, betaExchanger, gammaExchanger);

        CreditInstitution creditCafe = createCreditable("CreditCafe", "Somewhere 8", 4000, 200);
        CreditInstitution creditUnion = createCreditable("CreditUnion", "Somewhere 9", 100000, 20);
        CreditInstitution pownShop = createCreditable("PawnShop", "Somewhere 7", 50000, 40);
        List<CreditInstitution> cretitables = Arrays.asList(creditCafe, creditUnion, pownShop);

        OpenEndFund openEndFund = new OpenEndFund();
        openEndFund.setOrganizationInfo(new OpenEndFundInfo("OpenEndFund", "Somewhere 10", 1998));

        PostOffice postOffice = new PostOffice();
        postOffice.setOrganizationInfo(new BaseInfo("PostOffice", "Somewhere 11"));

        financeSystem.getCreditables().addAll(banks);
        financeSystem.getCreditables().addAll(cretitables);

        financeSystem.getDepositables().addAll(banks);
        financeSystem.getDepositables().add(openEndFund);

        financeSystem.getExchangebles().addAll(banks);
        financeSystem.getExchangebles().addAll(currencyExchangers);

        financeSystem.getSenders().addAll(banks);
        financeSystem.getSenders().add(postOffice);

        return financeSystem;
    }

    private CreditInstitution createCreditable(String name, String address, int maxValue, int percent) {
        CreditInstitution creditable = new CreditInstitution(maxValue, percent);
        creditable.setOrganizationInfo(new BaseInfo(name, address));
        return creditable;
    }

    private CurrencyExchanger createExchanger(String name, String address, float rateEUR, float rateUSD, float rateRu) {
        CurrencyExchanger currencyExchanger = new CurrencyExchanger();
        currencyExchanger.setOrganizationInfo(new BaseInfo(name, address));
        currencyExchanger.updateDailyRate(new DailyRate(rateEUR, rateUSD, rateRu));
        return currencyExchanger;
    }

    private Bank createBank(String name, String address, float rateEUR, float rateUSD, float rateRU) {
        Bank bank = new Bank();
        bank.setOrganizationInfo(new BaseInfo(name, address));
        bank.updateDailyRate(new DailyRate(rateEUR, rateUSD, rateRU));
        return bank;
    }


}
