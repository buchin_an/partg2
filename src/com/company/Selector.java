package com.company;

import java.util.Scanner;

public final class Selector {

    public static final int EUR = 1;
    public static final int USD = 2;
    public static final int RU = 3;
    public static final int UAH = 4;

    int amount;
    int number;
    boolean isUa;

    public void select() {
        FinanceSystemGenerator financeSystemGenerator = new FinanceSystemGenerator();
        FinanceSystem financeSystem = financeSystemGenerator.generate();

        System.out.println("Choose operation (only number)");
        System.out.println("1 Currency exchange");
        System.out.println("2 Take a loan");
        System.out.println("3 Deposit");
        System.out.println("4 Money order");

        Scanner scanner = new Scanner(System.in);
        int choice = Integer.parseInt(scanner.nextLine());
        switch (choice) {
            case 1:
                System.out.println("Select your currency");
                System.out.println("1 EUR");
                System.out.println("2 USD");
                System.out.println("3 RU");
                System.out.println("4 UAH");
                int currencyChoice = Integer.parseInt(scanner.nextLine());
                switch (currencyChoice) {
                    case EUR:
                        isUa = false;
                        System.out.println("Enter amount");
                        amount = Integer.parseInt(scanner.nextLine());
                        financeSystem.convert(amount, EUR, isUa);
                        break;
                    case USD:
                        isUa = false;
                        System.out.println("Enter amount");
                        amount = Integer.parseInt(scanner.nextLine());
                        financeSystem.convert(amount, USD, isUa);
                        break;
                    case RU:
                        isUa = false;
                        System.out.println("Enter amount");
                        amount = Integer.parseInt(scanner.nextLine());
                        financeSystem.convert(amount, RU, isUa);
                        break;
                    case UAH:
                        isUa = true;
                        System.out.println("Select target currency");
                        System.out.println("1 EUR");
                        System.out.println("2 USD");
                        System.out.println("3 RU");
                        int currencyTarget = Integer.parseInt(scanner.nextLine());
                        switch (currencyTarget) {
                            case EUR:
                                System.out.println("Enter amount");
                                amount = Integer.parseInt(scanner.nextLine());
                                financeSystem.convert(amount, EUR, isUa);
                                break;
                            case USD:
                                System.out.println("Enter amount");
                                amount = Integer.parseInt(scanner.nextLine());
                                financeSystem.convert(amount, USD, isUa);
                                break;
                            case RU:
                                System.out.println("Enter amount");
                                amount = Integer.parseInt(scanner.nextLine());
                                financeSystem.convert(amount, RU, isUa);
                                break;
                        }
                        break;

                }
                break;
            case 2:
                System.out.println("Enter amount");
                amount = Integer.parseInt(scanner.nextLine());
                System.out.println("Enter number month");
                number = Integer.parseInt(scanner.nextLine());
                financeSystem.credit(amount, number);
                break;
            case 3:
                System.out.println("Enter amount");
                amount = Integer.parseInt(scanner.nextLine());
                System.out.println("Enter number month");
                number = Integer.parseInt(scanner.nextLine());
                financeSystem.deposite(amount, number);
                break;
            case 4:
                System.out.println("Enter amount");
                amount = Integer.parseInt(scanner.nextLine());
                financeSystem.send(amount);
                break;
        }
    }
}
