package com.company.model;

public interface Creditable {

    double credit(int amount, int number);

}
