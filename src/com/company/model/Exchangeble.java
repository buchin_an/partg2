package com.company.model;

import com.company.model.impl.DailyRate;

public interface Exchangeble {


    double convert(int amount, int choice, boolean isUA);

    void updateDailyRate(DailyRate dailyRate);


}
