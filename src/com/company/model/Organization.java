package com.company.model;

import com.company.model.information.OrganizationInfo;

public class Organization {

    private OrganizationInfo organizationInfo;

    public OrganizationInfo getOrganizationInfo() {
        return organizationInfo;
    }

    public void setOrganizationInfo(OrganizationInfo organizationInfo) {
        this.organizationInfo = organizationInfo;
    }
}
