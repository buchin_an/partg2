package com.company.model;

public interface Sender {

    double send(int amount);

}
