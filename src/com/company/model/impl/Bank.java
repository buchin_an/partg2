package com.company.model.impl;

import com.company.model.*;

public class Bank extends Organization implements Exchangeble, Creditable, Depositable, Sender {

    private Exchanger exchanger;

    private int maxValueExchange   = 12000;
    private int commissionExchange = 15;

    private int maxValueCredit = 200_000;
    private int percent        = 25;
    private int depositeNumber = 12;

    public Bank() {
        exchanger = new Exchanger();
    }

    @Override
    public double convert(int amount, int choice, boolean isUA) {
        if (isUA) {
            if (amount < maxValueExchange) {
                return exchanger.convert(amount - commissionExchange, choice, isUA);
            } else return 0;
        }
        return exchanger.convert(amount, choice, isUA) - commissionExchange;
    }

    @Override
    public double credit(int amount, int number) {
        if (amount < maxValueCredit) {
            return percent / 12 * number;
        }
        return Double.MAX_VALUE;
    }

    @Override
    public double deposite(int amount, int number) {
        if (number < depositeNumber) {
            return percent / 12;
        }
        return 0;
    }

    @Override
    public double send(int amount) {
        return amount * .99 - 5;
    }

    @Override
    public void updateDailyRate(DailyRate dailyRate) {
        exchanger.updateDailyRate(dailyRate);
    }

}
