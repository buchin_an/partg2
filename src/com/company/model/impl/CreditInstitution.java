package com.company.model.impl;

import com.company.model.Creditable;
import com.company.model.Organization;

public class CreditInstitution extends Organization implements Creditable {

    private int maxValue;
    private int percent;

    public CreditInstitution(int maxValue, int percent) {
        this.maxValue = maxValue;
        this.percent = percent;
    }

    @Override
    public double credit(int amount, int number) {
        if (amount < maxValue) {
            return percent / 12 * number;
        }
        return Double.MAX_VALUE;
    }

}
