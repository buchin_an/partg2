package com.company.model.impl;

import com.company.model.Exchangeble;
import com.company.model.Organization;

public class CurrencyExchanger extends Organization implements Exchangeble {

    private Exchanger exchanger;

    public CurrencyExchanger() {
        exchanger = new Exchanger();
    }

    @Override
    public double convert(int amount, int choice, boolean isUA) {
        return exchanger.convert(amount, choice, isUA);
    }

    @Override
    public void updateDailyRate(DailyRate dailyRate) {
        exchanger.updateDailyRate(dailyRate);
    }

}
