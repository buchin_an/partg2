package com.company.model.impl;

public final class DailyRate {

    private float rateEUR;
    private float rateUSD;
    private float rateRU;

    public DailyRate(float rateEUR, float rateUSD, float rateRU) {
        this.rateEUR = rateEUR;
        this.rateUSD = rateUSD;
        this.rateRU = rateRU;
    }

    public float getRateEUR() {
        return rateEUR;
    }

    public float getRateUSD() {
        return rateUSD;
    }

    public float getRateRU() {
        return rateRU;
    }
}
