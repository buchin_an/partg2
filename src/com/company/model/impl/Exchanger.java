package com.company.model.impl;

import com.company.Selector;

public class Exchanger {

    private DailyRate dailyRate;

    public double convert(int amount, int choice, boolean isUA) {
        float rateEUR = dailyRate.getRateEUR();
        float rateUSD = dailyRate.getRateUSD();
        float rateRU = dailyRate.getRateRU();

        if (!isUA) {
            switch (choice) {
                case Selector.EUR:
                    return amount * rateEUR;
                case Selector.USD:
                    return amount * rateUSD;
                case Selector.RU:
                    return amount * rateRU;
            }
        }
        if (isUA) {
            switch (choice) {
                case Selector.EUR:
                    return amount / rateEUR;
                case Selector.USD:
                    return amount / rateUSD;
                case Selector.RU:
                    return amount / rateRU;
            }
        }
        return 0;

    }

    public void updateDailyRate(DailyRate dailyRate) {
        this.dailyRate = dailyRate;
    }
}
