package com.company.model.impl;

import com.company.model.Depositable;
import com.company.model.Organization;

public class OpenEndFund extends Organization implements Depositable {

    private int minNumber = 11;
    private int percent = 20;


    @Override
    public double deposite(int amount, int number) {
        if (number > minNumber) {
            return percent / 12;
        }
        return 0;
    }

}
