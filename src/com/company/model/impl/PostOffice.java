package com.company.model.impl;

import com.company.model.Organization;
import com.company.model.Sender;

public class PostOffice extends Organization implements Sender {


    @Override
    public double send(int amount) {

        return amount * .98;
    }


}
