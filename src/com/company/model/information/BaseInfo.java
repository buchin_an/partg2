package com.company.model.information;

public class BaseInfo extends OrganizationInfo {

    public BaseInfo(String name, String address) {
        super(name, address);
    }

    @Override
    public String formattedValue() {
        return "BaseInfo{" +
                "name='" + name + '\'' +
                ", address='" + address + '\'' +
                "} ";
    }
}
