package com.company.model.information;

public class OpenEndFundInfo extends OrganizationInfo {

    private int date;

    public OpenEndFundInfo(String name, String address, int date) {
        super(name, address);
        this.date = date;
    }

    @Override
    public String formattedValue() {
        return "OpenEndFundInfo{" +
                "date=" + getDate() +
                ", name='" + getName() + '\'' +
                ", address='" + getAddress() + '\'' +
                "} ";
    }

    public int getDate() {
        return date;
    }
}
