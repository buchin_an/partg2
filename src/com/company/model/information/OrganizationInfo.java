package com.company.model.information;


public abstract class OrganizationInfo {

    private String name;
    private String address;

    public OrganizationInfo(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public abstract String formattedValue();

}
